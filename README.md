MessageParser
==================

This will take a message and extract out mentions, emoticons, and links.
Links will be fetched to extract their title.
This is nodejs only due to the Same origin policy blocking requests to other domains to get their titles.

Installation
---------------
npm install

Run tests with jasmine
