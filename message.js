var cheerio = require("cheerio");
var request = require("request");

/**
 * Takes a string and extracts out information
 * @param {string} message
 * @returns {object} Object containing optional keys: mentions, emoticons, links
 */
function parseMessage(message) {
  // mentions
  var mentionsRegexp = /\@(\w+)/g,
      mentions = findMatches(mentionsRegexp, message);

  // emoticons
  var emoticonsRegexp = /\((\w{1,15})\)/g,
      emoticons = findMatches(emoticonsRegexp, message);

  // urls
  var urlsRegexp = /((ftp|https?|file):\/\/[^ "]+)/g,
      urls = findMatches(urlsRegexp, message);

  return new Promise(function(resolve, reject) {
    Promise.all(urls.map(fetchTitle)).then(function(links){
      var result = {};

      if (mentions.length) {
        result.mentions = mentions;
      }

      if (emoticons.length) {
        result.emoticons = emoticons;
      }

      if (urls.length) {
        result.links = links;
      }

      resolve(JSON.stringify(result));
    });
  });
}
module.exports.parse = parseMessage;

/**
 * Returns all matches for the regex in the message
 * @param {regexp} regExp Should contain a capture and be global
 * @param {string} message
 * @returns {array} Captured text from the message
 */
function findMatches(regExp, message) {
  var matches = [],
      match;

  while (match = regExp.exec(message)) {
    matches.push(match[1]);
  }
  return matches;
}

/**
 * Request the url and extract the title from the document
 * returns no title in failure case
 * @param {string} url
 * @returns {object} Object containing keys for the url and title
 */
function fetchTitle(url) {
  return new Promise(function(resolve, reject) {
    request(url, function(error, response, body){
      if (!error && response.statusCode == 200) {
        // We'll use cheerio to grab the title from the document
        $ = cheerio.load(body);
        resolve({url: url, title: $('title').text()});
      } else {
        // Resolve either way, title is null if it could not be grabbed
        resolve({url: url, title: null});
      }
    });
  });
}
module.exports.fetchTitle = fetchTitle;
