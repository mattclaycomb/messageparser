var message = require("../message.js");

describe("MessageParser", function() {
  it("should catch mentions", function(done) {
    message.parse("Hey @bill(happy), did you talk to @John?").then(function(results) {
      results = JSON.parse(results);

      expect(results.mentions[0]).toBe("bill");
      expect(results.mentions[1]).toBe("John");
      expect(results.mentions.length).toBe(2);
      done();
    });
  });

  it("should catch emoticons ", function(done) {
    message.parse("Hey @bill(happy), did you talk to @John?").then(function(results) {
      results = JSON.parse(results);

      expect(results.emoticons[0]).toBe("happy");
      expect(results.emoticons.length).toBe(1);
      done();
    });
  });

  it("should not catch emoticons with spaces or are empty", function(done) {
    message.parse("Hey (happy face)! How are () you?").then(function(results){
      results = JSON.parse(results);

      expect(results.emoticons).not.toBeDefined;
      done();
    });
  });

  it("should not catch emoticons longer that 15 characters", function(done) {
    message.parse("Hey (thisisalittletoolong)!").then(function(results){
      results = JSON.parse(results);

      expect(results.emoticons).toBeUndefined;
      done();
    });
  });

  // TODO: Switch to local resouces to avoid breakage of page titles
  it("should catch urls", function(done) {
    message.parse("Hey (happy face)! http://google.com Try:http://twitter.com").then(function(results) {
      results = JSON.parse(results);

      expect(results.links[0].title).toBe("Google");
      expect(results.links[1].title).toBe("Welcome to Twitter - Login or Sign up");
      done();
    });
  });

  it("should work on the provided example", function(done) {
    message.parse("@bob @john (success) such a cool feature; https://twitter.com/").then(function(results) {
      results = JSON.parse(results);

      expect(results.mentions[0]).toBe("bob");
      expect(results.mentions[1]).toBe("john");
      expect(results.mentions.length).toBe(2);
      expect(results.emoticons[0]).toBe("success");
      expect(results.emoticons.length).toBe(1);
      expect(results.links[0].url).toBe("https://twitter.com/")
      expect(results.links[0].title).toBe("Welcome to Twitter - Login or Sign up");
      expect(results.links.length).toBe(1);
      done();
    });
  })
});



// TODO: Switch to local resouces to avoid breakage of page titles
describe("fetchTitle", function() {
  it("should grab a title", function(done) {
    message.fetchTitle("http://jquery.com").then(function(link) {
      expect(link.url).toBe("http://jquery.com");
      expect(link.title).toBe("jQuery");
      done();
    });
  });

  it("should work when wrong", function(done) {
    message.fetchTitle("http://wrong").then(function(link) {
      expect(link.url).toBe("http://wrong");
      expect(link.title).toBeFalsey;
      done();
    });
  });
});
